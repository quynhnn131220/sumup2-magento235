<?php

namespace SU\Brand\Model;

use Magento\Framework\Model\AbstractModel;

class Brand extends  AbstractModel
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    protected function _construct()
    {
        $this->_init('SU\Brand\Model\ResourceModel\Brand');
    }

    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
}
