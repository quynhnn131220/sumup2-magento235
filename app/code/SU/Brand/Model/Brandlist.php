<?php
/**
 * Magetop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magetop.com license that is
 * available through the world-wide-web at this URL:
 * https://www.magetop.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Magetop
 * @package    Magetop_Brand
 * @copyright  Copyright (c) 2014 Magetop (https://www.magetop.com/)
 * @license    https://www.magetop.com/LICENSE.txt
 */
namespace SU\Brand\Model;

class Brandlist extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $_brand;

    /**
     *
     * @param \SU\Brand\Model\Brand $brand
     */
    public function __construct(
        \SU\Brand\Model\Brand $brand
    ) {
        $this->_brand = $brand;
    }

    /**
     * Get Gift Card available templates
     *
     * @return array
     */
    public function getAvailableTemplate()
    {
        $brands = $this->_brand->getCollection()
            ->addFieldToFilter('status', '1');
        $listBrand = [];
        foreach ($brands as $brand) {
            $listBrand[] = ['label' => $brand->getName(),
                'value' => $brand->getId()];
        }
        return $listBrand;
    }

    /**
     * Get model option as array
     *
     * @return array
     */
    public function getAllOptions($withEmpty = true)
    {
        $options = [];
        $options = $this->getAvailableTemplate();

        if ($withEmpty) {
            array_unshift($options, [
                'value' => '',
                'label' => '-- Please Select --',
            ]);
        }
        return $options;
    }
}
