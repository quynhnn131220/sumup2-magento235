<?php

namespace SU\Brand\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Product\Action as ProductAction;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use SU\Brand\Model\BrandFactory;
use SU\Brand\Model\BrandProductFactory;

class Save extends Action
{
    protected $brandFactory;
    protected $brandProductFactory;
    protected $_updateAction;
    protected $_resource;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        Action\Context $context,
        UrlRewriteFactory $urlRewriteFactory,
        BrandFactory $brandFactory,
        BrandProductFactory $brandProductFactory,
        ProductAction $productAction
    ) {
        parent::__construct($context);
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->brandFactory = $brandFactory;
        $this->brandProductFactory = $brandProductFactory;
        $this->_updateAction = $productAction;
        $this->_resource = $resource;
    }

    public function execute()
    {
        $brand = $this->brandFactory->create();
        $data = $this->getRequest()->getPostValue();
        $data['name'] = trim($data['name']);
        $data['url_key'] = trim($data['url_key']);

        $id = !empty($data['id']) ? $data['id'] : null;
        $logoBrand = !empty($data['logo']) ? $data['logo'][0]['name'] : "default-image.png";
        $bannerBrand = !empty($data['banner']) ? $data['banner'][0]['name'] : "";

        $checkName = $brand->load($data['name'], 'name');

        if (!empty($checkName->getId())) {
            if ($id) {
                $checkBrandEditName = $brand->load($id);
                if ($checkBrandEditName->getName() != $data['name']) {
                    $this->getMessageManager()->addErrorMessage(__('Name brand already exists.'));
                    return $this->_redirect("shopbybrand/brand/edit/id/$id");
                }
            } else {
                $this->getMessageManager()->addErrorMessage(__('Name brand already exists.'));
                return $this->_redirect('shopbybrand/brand/add');
            }
        }

        if (!empty($data['url_key'])) {
            $url_key = trim($this->slug($data['url_key']), '-');
            $checkBrandEditUrlKey = $this->brandFactory->create()->getCollection()
                ->addFieldToFilter('url_key', "$url_key");

            if ($checkBrandEditUrlKey->count() > 0) {
                if ($id) {
                    foreach ($checkBrandEditUrlKey as $item) {
                        if ($item->getId() != $id) {
                            $this->getMessageManager()->addErrorMessage(__('Url Key brand already exists.'));
                            return $this->_redirect("shopbybrand/brand/edit/id/$id");
                        }
                    }
                } else {
                    $this->getMessageManager()->addErrorMessage(__('Url Key brand already exists.'));
                    return $this->_redirect('shopbybrand/brand/add');
                }
            }
        } else {
            $url_key = $this->slug($data['name']);
        }

        $newData = [
            'name' => $data['name'],
            'url_key' => $url_key,
            'status' => $data['status'],
            'is_feature' => $data['is_feature'],
            'short_description' => $data['short_description'],
            'description' => $data['description'],
            'logo' => $logoBrand,
            'banner' => $bannerBrand
        ];

        if ($id) {
            // url rewrite
            $urlRewriteModel = $this->_urlRewriteFactory->create()->load("/shopbybrand/brand/view/id/$id", 'target_path');

            $urlRewriteModel->setStoreId(1);
            $urlRewriteModel->setIsSystem(0);
            $urlRewriteModel->setIdPath(rand(1, 100000));
            $urlRewriteModel->getEntityId($id);
            $urlRewriteModel->setEntityType("Brand");
            $urlRewriteModel->setTargetPath("/shopbybrand/brand/view/id/$id");
            $urlRewriteModel->setRequestPath("$url_key.html");
            $urlRewriteModel->save();

            $brand->load($id);
            $this->getMessageManager()->addSuccessMessage(__('You add the brand.'));
        } else {
            $this->getMessageManager()->addSuccessMessage(__('You saved the brand.'));
        }
        try {
            $brand->addData($newData);
            $brand->save();
            $brandId = $brand->getId();

            // url rewrite
            if ($id == null) {
                $urlRewriteModel = $this->_urlRewriteFactory->create();
                $urlRewriteModel->setStoreId(1);
                $urlRewriteModel->setIsSystem(0);
                $urlRewriteModel->setEntityType("Brand");
                $urlRewriteModel->getEntityId($brandId);
                $urlRewriteModel->setIdPath(rand(1, 100000));
                $urlRewriteModel->setTargetPath("/shopbybrand/brand/view/id/$brandId");
                $urlRewriteModel->setRequestPath("$url_key.html");
                $urlRewriteModel->save();
            }

            $this->saveProductInBrand($data, $id, $brandId);

            return $this->_redirect('shopbybrand/brand/index');
        } catch (\Exception $e) {
            $this->getMessageManager()->addErrorMessage(__('You false the brand.'));
        }
    }

    public function saveProductInBrand($data, $idBrand, $brandSaveLastId)
    {
        $brandProduct = $this->brandProductFactory->create();

        if ($idBrand) {
            $productBrandCollection= $brandProduct->getCollection();

            foreach ($productBrandCollection as $item) {
                if ($idBrand == $item['brand_id']) {
                    $brandProduct->load($idBrand, 'brand_id');
                    $brandProduct->delete();
                }
            }
        }

        $brandProduct = $this->brandProductFactory->create();
        $dataBrandInProduct = isset($data['data']['links']['product']) ? $data['data']['links']['product'] : null;

        $connection = $this->_resource->getConnection();
        $table_name = $this->_resource->getTableName('catalog_product_entity_varchar');
        $connection->query('DELETE FROM ' . $table_name . ' WHERE value =  ' . (int)$brandSaveLastId . ' ');

        if (isset($dataBrandInProduct)) {
            $productIds = array_column($dataBrandInProduct, 'id');
            $this->_updateAction->updateAttributes($productIds, ['brand_product' => $idBrand], 0);
            foreach ($dataBrandInProduct as $product) {
                $brandProduct->setData("product_id", $product['id']);
                $brandProduct->setData("brand_id", $brandSaveLastId);
                $brandProduct->save();
                $brandProduct->setData("id", null);
            }
        }
    }

    public function slug($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }
}
