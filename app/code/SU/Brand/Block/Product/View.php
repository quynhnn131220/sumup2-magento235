<?php
/**
 * Magetop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magetop.com license that is
 * available through the world-wide-web at this URL:
 * https://www.magetop.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Magetop
 * @package    Magetop_Brand
 * @copyright  Copyright (c) 2014 Magetop (https://www.magetop.com/)
 * @license    https://www.magetop.com/LICENSE.txt
 */
namespace SU\Brand\Block\Product;

use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use SU\Brand\Model\BrandProductFactory;

class View extends \Magento\Framework\View\Element\Template
{
    protected $_brandCollection;

    protected $_coreRegistry = null;

    protected $_brandHelper;

    protected $_resource;

    protected $brandProductFactory;

    protected $storeManager;

    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \SU\Brand\Model\Brand $brandCollection,
        \Magento\Framework\App\ResourceConnection $resource,
        StoreManagerInterface $storeManager,
        BrandProductFactory $brandProductFactory,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->_brandCollection = $brandCollection;
        $this->_coreRegistry = $registry;
        $this->_resource = $resource;
        $this->brandProductFactory = $brandProductFactory;
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }

    public function getBrandCollection()
    {
        $product = $this->getProduct();
        $brandId  = $this->brandProductFactory->create()->load($product->getId(),'product_id')->getBrandId();

        if (!empty($brandId)) {
            $collection = $this->_brandCollection->getCollection()
                ->addFieldToFilter('status', 1);
            $collection->getSelect()->where("id = $brandId");
            return $collection;
        }
        return false;
    }


    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }
}
